#include "stm32f10x_gpio.h"
#include "misc.h"
#include "hzs.h"
#include "rtthread.h"
#include "App_LED_Blink.h"
#include "App_Breath.h"

//RT-Thread 调用的第一个线程，一般用它来启动其它线程
int main(void)
{
	rt_thread_t tid;
	
	tid = rt_thread_create("LED_Breath", App_Breath, (void*)0,Breath_STACK_SIZE,Breath_PRIORITY,Breath_TIMESLICE);
  if (tid != RT_NULL)
     rt_thread_startup(tid);	
	
	tid = rt_thread_create("LED_Blink", App_LED_Blink, (void*)0,LED_Blink_STACK_SIZE,LED_Blink_PRIORITY,LED_Blink_TIMESLICE);
  if (tid != RT_NULL)
     rt_thread_startup(tid);
	
	
	
	while(1)
	{
		rt_thread_mdelay(10000);	
	}
}

