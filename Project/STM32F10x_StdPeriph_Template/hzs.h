#ifndef _HZS_H
#define _HZS_H


#include"stm32f10x_rcc.h"
#include"stm32f10x_gpio.h"
#include"misc.h"
#include"stm32f10x_usart.h"

#define FlashBase (0x08008000)     //暂时选用后一半的flash作为用户存储，实际需观察程序区域开销
#define FlashData_ ((uint16_t *)(FlashBase))
#define FlashData (*FlashData_)


int my_init(void);
int delay(unsigned int);
int Timer2_init(void );
int my_RCC_init(void);
int my_Flash_init(void);
int my_Usart_init(void);
int my_GPIO_init(void);
int my_Nvic_init(void);
int my_SysTick_init(void);
uint16_t my_flash_Read(unsigned int Addr);
int my_flash_Write(unsigned int Addr,uint16_t data);
int my_Page_erase(unsigned int Page_Num);

#endif
