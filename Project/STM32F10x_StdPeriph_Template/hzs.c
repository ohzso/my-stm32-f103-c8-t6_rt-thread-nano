#include"hzs.h"
#include <rtthread.h>
#include <shell.h> //何曾胜2020-9-28增加对RT_thread/Fish控制台的支持

unsigned int irq_flag=1;
//volatile uint16_t usart_char='A';
unsigned int my_timer=0;
volatile uint32_t SysTick_ms=0;//来自systick生成的1ms向上计数器



int delay(unsigned int i)
{
//universal delay function,it is simple and not accurate !!!
	unsigned int m;
	unsigned int n;
	for(m=i;m>0;m--)
		for(n=i;n>0;n--);
	return 0;
}


void EXTI15_10_IRQHandler()
{
//here is the exit15 irq server function

	EXTI_ClearITPendingBit(EXTI_Line15);
	delay(100);
	if(!(0x01&GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_15)))
			irq_flag++;
}



int my_GPIO_init(void)
{
	
	GPIO_InitTypeDef GPIO_Init_struct_temp;
//	EXTI_InitTypeDef EXTI_InitStruct_temp;
//	NVIC_InitTypeDef NVIC_InitStruct_temp;

//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);//we need to use gpio's interrupt,so enable the AFIO clock;

//herer we light up the Led on core board to tell others that we have fired up the system successfully,at least once;
//init the gpio port c 13 as input as output push and pull at 50Mhz
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_DeInit (GPIOB);
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_11;
	GPIO_Init(GPIOB, &GPIO_Init_struct_temp);	
	GPIO_SetBits(GPIOB,GPIO_Pin_11);
	
/*	
//init the gpio port c 15 as input with up pull
//GPIO_DeInit (GPIOC);
	GPIO_DeInit (GPIOC);
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_15;
	GPIO_Init(GPIOC, &GPIO_Init_struct_temp);
//open the irq of pc.15
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource15);
//here we config the exit controler ,choose line15 which is connected to gpio.15s.
	EXTI_DeInit();
	EXTI_InitStruct_temp.EXTI_Line=EXTI_Line15;
	EXTI_InitStruct_temp.EXTI_LineCmd=ENABLE;
	EXTI_InitStruct_temp.EXTI_Mode=EXTI_Mode_Interrupt;//not event mode,we choose interrupt mode
	EXTI_InitStruct_temp.EXTI_Trigger=EXTI_Trigger_Falling;//falling edge trigger
	EXTI_Init(&EXTI_InitStruct_temp);
//here we config the NVIC registers.
	NVIC_InitStruct_temp.NVIC_IRQChannel=EXTI15_10_IRQn;
	NVIC_InitStruct_temp.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct_temp.NVIC_IRQChannelPreemptionPriority=2;
	NVIC_InitStruct_temp.NVIC_IRQChannelSubPriority=2;
	NVIC_Init(&NVIC_InitStruct_temp);

	

	
	
	//init the gpio porta.0 as output push and pull,timer2_ch1 pwm output pin
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_0;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);
	
	
	//init the gpio porta.1 as output push and pull,timer2_ch2 pwm output pin
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_1;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);
	
	
	//init the gpio porta.2 as output push and pull,timer2_ch3 pwm output pin
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_2;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);
*/




	//here open the gpioa and usart1,as they two use the same output pins	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	
	//init the gpio porta.9 as push and pull output by phri device,uart's Tx pin
	GPIO_DeInit(GPIOA);
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_9;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);
	
	//init the gpio porta.10 as input floating mode by phri device,uart's Rx pin
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_IN_FLOATING;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_10;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);
	
	return 0;
}

INIT_BOARD_EXPORT(my_GPIO_init);


int my_Nvic_init(void)
{
	//here we do some universe operation about nvic controllor
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//group the interrupts here we set it as 2 pre and 2 sub
	return 0;
}
INIT_BOARD_EXPORT(my_Nvic_init);





int Timer2_init()
{
//here we config the registers about timer6,enable it's interrupt,to generation a clock of 1s
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct_temp;
	NVIC_InitTypeDef NVIC_InitStruct_temp;
	TIM_OCInitTypeDef TIM_OCInitStruct_temp;
		
	TIM_DeInit(TIM2);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	
//	TIM_TimeBaseInitStruct_temp.TIM_ClockDivision=TIM_CKD_DIV1; This is for input trip mode
	TIM_TimeBaseInitStruct_temp.TIM_CounterMode=TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct_temp.TIM_Period=256;
	TIM_TimeBaseInitStruct_temp.TIM_Prescaler=50;
//	TIM_TimeBaseInitStruct_temp.TIM_RepetitionCounter=;  This is for Tim1 and Tim8
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStruct_temp);
	
	TIM_ARRPreloadConfig(TIM2, ENABLE);	
	
	TIM_Cmd(TIM2,ENABLE);	
	
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
	
	NVIC_InitStruct_temp.NVIC_IRQChannel= TIM2_IRQn;
	NVIC_InitStruct_temp.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct_temp.NVIC_IRQChannelPreemptionPriority=2;
	NVIC_InitStruct_temp.NVIC_IRQChannelSubPriority=1;
	NVIC_Init(&NVIC_InitStruct_temp);
	
	
	TIM_OCStructInit(&TIM_OCInitStruct_temp);
//	TIM_OCInitStruct_temp.TIM_OCIdleState=                for timer1,timer8
//	TIM_OCInitStruct_temp.TIM_OCNIdleState=
//	TIM_OCInitStruct_temp.TIM_OCNPolarity=
//	TIM_OCInitStruct_temp.TIM_OutputNState=
//	TIM_CCPreloadControl(TIM2, ENABLE);

/*通道1设置*/
	TIM_OCInitStruct_temp.TIM_OCMode=TIM_OCMode_PWM1;
	TIM_OCInitStruct_temp.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OCInitStruct_temp.TIM_OutputState=TIM_OutputState_Enable;
	TIM_OCInitStruct_temp.TIM_Pulse=120;//here we can set the duty percent 50%
	TIM_OC1Init(TIM2, &TIM_OCInitStruct_temp);	
	TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Enable);	
	TIM_CCxCmd(TIM2, TIM_Channel_1, TIM_CCx_Enable);//



/*通道2设置*/
	TIM_OCInitStruct_temp.TIM_OCMode=TIM_OCMode_PWM1;
	TIM_OCInitStruct_temp.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OCInitStruct_temp.TIM_OutputState=TIM_OutputState_Enable;
	TIM_OCInitStruct_temp.TIM_Pulse=110;//here we can set the duty percent 50%
	TIM_OC2Init(TIM2, &TIM_OCInitStruct_temp);	
	TIM_OC2PreloadConfig(TIM2, TIM_OCPreload_Enable);		
	TIM_CCxCmd(TIM2, TIM_Channel_2, TIM_CCx_Enable);//

/*通道3设置*/
	TIM_OCInitStruct_temp.TIM_OCMode=TIM_OCMode_PWM1;
	TIM_OCInitStruct_temp.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OCInitStruct_temp.TIM_OutputState=TIM_OutputState_Enable;
	TIM_OCInitStruct_temp.TIM_Pulse=38;//here we can set the duty percent 50%
	TIM_OC3Init(TIM2, &TIM_OCInitStruct_temp);	
	TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);	
	TIM_CCxCmd(TIM2, TIM_Channel_3, TIM_CCx_Enable);//
		
//	TIM_GenerateEvent(TIM2, TIM_EventSource_CC2);	
	
	return 0;	
}

void TIM2_IRQHandler()
{	
	TIM_ClearITPendingBit(TIM2,TIM_IT_Update);	
	my_timer++;
//	uart1_printf("\r\n here is TIM2 IRQ Hnadler \r\n",100);	
}


int my_RCC_init(void)
{
	
	RCC_DeInit();	
	RCC_HSEConfig(RCC_HSE_ON);
	RCC_WaitForHSEStartUp();
	RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9); //8X9=72MHz,
	RCC_PLLCmd(ENABLE);
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);//choose pll as clock source
	RCC_HCLKConfig(RCC_SYSCLK_Div1);//set AHB as 72Mhz
	RCC_PCLK1Config(RCC_HCLK_Div2);//set AHB1 Low speed as 36MHz,RCC_HCLK_Div2
	RCC_PCLK2Config(RCC_HCLK_Div1);//set AHB2 High speed as 72MHz
	while(RCC_GetSYSCLKSource() != 0x08);//等待PLL稳定，作为系统时钟
	/*SystemInit ();*/
	return 0;
}

int my_Flash_init(void)
{	
	FLASH_SetLatency(FLASH_Latency_2);
	return 0;
}





/*RT_thread 有现成代码，j接管此部分工作
int my_SysTick_init(void)
{
	//init the SysTick to generate a pulse of 1ms
	
	
	while(SysTick_Config(9000)==1);//等待systick设置成功
	
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);//无效果，奇怪。2019-12-3，可以，之前一个参数写错了，造成错误设置
	return 0;
	
}
*/

/*RT-thread已经接管此函数，删除自己的
int SysTick_Handler(void)
{
	SysTick_ms++;
	return 0;
}
*/

/* 此废弃，使用rtthread 自动初始化机制，直接在初始化函数后面加宏
int my_init(void)
{
//	my_RCC_init();
//	my_SysTick_init();
//	my_Nvic_init();

//	my_Usart_init();
//	Timer2_init();
  my_Flash_init();
	return 0;
}
*/



uint16_t my_flash_Read(unsigned int Addr)
{
	//Addr 是偏移地址，基地址是FlashBase，这个宏应该根据实际程序大小来决定，要防止覆盖程序存储空间
	return *((uint16_t *)((Addr<<1)+FlashBase));//因为dgus屏幕的地址是紧密排列的，但是flash每次读写2字节，所以地址乘以2
}

int my_flash_Write(unsigned int Addr,uint16_t data)
{
//	FLASH_Unlock();
	FLASH_ProgramHalfWord((Addr<<1)+FlashBase,data);//因为dgus屏幕的地址是紧密排列的，但是flash每次读写2字节，所以地址乘以2
//	FLASH_Lock();
	return 0;
}


int my_Page_erase(unsigned int Page_Num)
{
	//擦除一整页flash，根据flash的特性，必须整页擦除后，才能进行写入操作
//	FLASH_Unlock();
	FLASH_ErasePage(FlashBase+((Page_Num-1)<<10));
//	FLASH_Lock();
	return 0;
}




/*2020-10-1添加代码，实现串口缓冲：
1计划采用一个125字节的数组：Char UartBuffer[125]，
2另外编制一个叫BufferNext()的函数，用来获取下一个节点的下标号，0-123时，对应的下一个节点是n+1，当n等于124时，返回的
下一个节点等于0，从而实现环形缓冲。
3实现环形缓冲结构，每次串口中断读取新字符，保存在环形缓冲中
保存函数维护一个Last、一个Empty变量。Last用以标志最近的未读字符，Empty用以标志最近的空位。
4通过一个Semaphore变量：CharValid，指示缓冲中的有效数据量，当等于0的时候，新的读取操作，会使线程被暂停，知道串口中断获取并更新新的数据。
5结合RT-Therd，修改rt_hw_console_getchar（），当其读取buffer时，先检查CharValid，如果是0，则暂时挂起调用的线程，直到有新字符进入。
*/
#define BUFFER_SIZE 128
static char UartBuffer[BUFFER_SIZE];//Uart的接收缓存
static unsigned char BufferNext(unsigned char i);//获取下一个节点
static unsigned char Last=0,Empty=0;//未读字符位置，最近空位
static struct rt_semaphore SemUartRx;//信号量，初始化存量为0，每接收一个字符，则+1；

unsigned char BufferNext(unsigned char i)
{
	unsigned char n;
	if(i<BUFFER_SIZE-1)
	 n=i+1;
	if(i==BUFFER_SIZE-1)
		n=0;
	return n;
}




int  USART1_IRQHandler()
{
	rt_interrupt_enter();

//here we deal with uart1 irq
	
		
	//接收处理
	if(USART_GetITStatus(USART1, USART_IT_RXNE))
	{

		USART_ClearITPendingBit(USART1, USART_IT_RXNE);
		UartBuffer[Empty]=(unsigned char)USART_ReceiveData(USART1);
		Empty= BufferNext(Empty);
		rt_sem_release(&SemUartRx);
	}
/*
	//发送处理
		if(USART_GetFlagStatus(USART1, USART_IT_TXE))
	{
		USART_ClearITPendingBit(USART1, USART_IT_TXE);
		rt_sem_release(&SemUartTx);
	}
		*/
	rt_interrupt_leave();
	return 0;
}

unsigned char UartBufferGetChar()//带阻塞的getChar函数
{
	unsigned char temp;
	while(rt_sem_take(&SemUartRx, 10)==-RT_ETIMEOUT);
	temp=UartBuffer[Last];
	Last=BufferNext(Last);
	return temp;
}



int my_Usart_init(void)
{
	USART_InitTypeDef USART_InitStruct_temp;
	USART_ClockInitTypeDef USART_ClockInitStruct_temp;
  NVIC_InitTypeDef NVIC_InitStruct_temp;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);// open the clock of usart1
//then we start to init the usart1
	USART_DeInit(USART1);
  USART_StructInit(&USART_InitStruct_temp);
	USART_InitStruct_temp.USART_BaudRate=115200;
	USART_Init(USART1, &USART_InitStruct_temp);
  USART_ClockStructInit(&USART_ClockInitStruct_temp);
	USART_ClockInit(USART1, &USART_ClockInitStruct_temp);
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE ); 
 // USART_ITConfig(USART1, USART_IT_TXE, ENABLE ); there is no need to use send ok irq,because we can decide this time.
//but if there is a need to run the cpu during sending message,I shuold use this interrupt to get back.
  USART_Cmd(USART1, ENABLE);
/*	*/
	NVIC_InitStruct_temp.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct_temp.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStruct_temp.NVIC_IRQChannelPreemptionPriority=3 ;//
	NVIC_InitStruct_temp.NVIC_IRQChannelSubPriority = 3;        //           //
	NVIC_Init(&NVIC_InitStruct_temp); 
	
	
	rt_sem_init(&SemUartRx,"SEM_RX",0,RT_IPC_FLAG_FIFO);

	return 0;
}
INIT_BOARD_EXPORT(my_Usart_init);





extern rt_tick_t rt_tick_get(void);
void ohzso(void)
{
	unsigned int ms,min,hour;
	ms=rt_tick_get();
	min=ms/60000;
	hour=min/60;
	rt_kprintf("\nRT-thread run time: hour:%d = min:%d = min:%d \nBy ohzso \n\n",hour,min,ms);
}

MSH_CMD_EXPORT(ohzso , say hello to RT-Thread mesh mode);



