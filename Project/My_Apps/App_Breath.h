#ifndef  _APP_BREATH
#define  _APP_BREATH


#include <rtthread.h>
#include"stm32f10x_gpio.h"

void App_Breath(void* parameter);

#define Breath_STACK_SIZE  128
#define Breath_PRIORITY    80
#define Breath_TIMESLICE   10



#endif
