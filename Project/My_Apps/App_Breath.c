#include "App_Breath.h"

extern unsigned char duty;
void App_Breath(void* parameter)
{
	unsigned char cycle_array[30];
	unsigned char i=0;//数组指针
	unsigned char dir=1;//方向标志
	cycle_array[0]=1;
	cycle_array[1]=1;
	cycle_array[2]=2;
	cycle_array[3]=2;
	cycle_array[4]=3;
	cycle_array[5]=3;
	cycle_array[6]=4;
	cycle_array[7]=4;
	cycle_array[8]=5;
	cycle_array[9]=5;
	cycle_array[10]=6;
	cycle_array[11]=6;
	cycle_array[12]=7;
	cycle_array[13]=7;
	cycle_array[14]=8;
	cycle_array[15]=8;
	cycle_array[16]=9;
	cycle_array[17]=9;
	cycle_array[18]=10;
	cycle_array[19]=10;
	cycle_array[20]=11;
	cycle_array[21]=11;
	cycle_array[22]=12;
	cycle_array[23]=12;
	cycle_array[24]=13;
	cycle_array[25]=13;
	cycle_array[26]=14;
	cycle_array[27]=14;
	cycle_array[28]=15;
	cycle_array[29]=15;	
	
  while (1)
  {			
		if(dir==0)
			i--;
		else
			i++;		
		if(i>=29)
			dir=0;
		if(i<=0)
			dir=1;		
		duty=cycle_array[i];
		rt_thread_mdelay(20);			
	}//while1结束
}


