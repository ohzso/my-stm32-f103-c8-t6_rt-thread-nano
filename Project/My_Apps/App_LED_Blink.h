#ifndef  _APP_LED_BLINK
#define  _APP_LED_BLINK


#include <rtthread.h>
#include"stm32f10x_gpio.h"

void App_LED_Blink(void* parameter);

#define LED_Blink_STACK_SIZE  128
#define LED_Blink_PRIORITY     81
#define LED_Blink_TIMESLICE   100


#define Cycle_time 19


#endif
